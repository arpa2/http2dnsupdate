# Dynamic DNS with SASL-Auth HTTP Front

> *Dynamic DNS updates are powerful, especially when they are access-controlled.
> Applications include the typical settings of IP addresses, but also admins
> who redirect services, including from a plugin service provider.*

This is a HTTP-wrapped format for
[Dynamic DNS Updates](https://www.rfc-editor.org/rfc/rfc2136).
Unlike in the DNS form, which is fragile in terms of security and
access control, the addition of
[HTTP-SASL](https://datatracker.ietf.org/doc/html/draft-vanrein-httpauth-sasl)
allows tight control over which users may edit which records.

Given a service running on a fixed address (and perhaps a fixed port),
it is easy to configure access control to the Dynamic DNS Update
facility of a name server.  This saves the name server from having
to implement a detailed user database with credentials.

## HTTP Gateway for RFC 2136

A simple mapping to a HTTP backend can be based on URL parameters,
as is common in most script languages.  Additional aspects may
be configured in environment variables, specifically when they
are considered internal and should be protected from modification
while in transit to the HTTP backend.

The following elements are used in the RFC 2136 specification:

  * The zone that should be modified
  * Prerequisites RRs or RRsets, both positive and negative
  * Updates in the form of RRs or RRsets to add and remove
  * Additional data

### CoAP Gateway

Since CoAP and HTTP are reasonably exchangable, it is possible to use the
same profile as described herein for a CoAP gateway.  The trivial
definition follows by considering the same translations as a
CoAP-to-HTTP gateway would follow.

### Query Strings

Dynamic DNS Updates are mapped to query strings for HTTP URIs.
The order of the parameters matters, and must start with the
Zone Class, Zone, then Prerequisites, the Updates and
optionally a list of Additional Data.

### Zone

This is not the DNS name but the zone that forms its start of
authority.  It is sent with a ZTYPE field set to SOA and a
ZCLASS that would default to IN.  In general, this word follows
the IANA-standardised uppercase form.  It may be explicitly
defined as `zclass=` argument.

Since opening up to other networks than IN would be a security
problem, any extra query parameter `zclass=` should be matched
by a positive list, perhaps on the commandline.  If such a list
is not given, it defaults to IN.  If the query parameter is not
given, it defaults to the first in the list, which normally
means that it defaults to IN.

The only required data is the zone name.  Since a web context
is usually under a domain, it is reasonable to use an
`ARPA2_DOMAIN` environment variable.  If this is not provided
a `zone=` query parameter may be used.  It is not generally
possible to know a zone from a website's server hostname.

The zone name for IN is represented as a fully qualified domain
in lowercase, with no trailing dot.  Further sections present
DNS owner names, which must have the zone labels as its last
labels; it may be the same as the zone.

Note how this definition supports software implementations
that require an `ARPA2_DOMAIN`.

Note how this definition supports software implementations
that ignore `zclass` and always use IN.

### Prerequisites

There are various requirements, each with their own DNS syntax.
We can map these to a textual syntax that is very close.

```
CLASS    TYPE     RDATA    Meaning
------------------------------------------------------------
ANY      ANY      empty    Name is in use
ANY      rrset    empty    RRset exists (value independent)
NONE     ANY      empty    Name is not in use
NONE     rrset    empty    RRset does not exist
zone     rrset    rr       RRset exists (value dependent)
```

The CLASS is set as follows:

  * NONE to indicate absense of a NAME (and possibly TYPE
    if not set to ANY)
  * ANY to indicate presence of a NAME (and possibly TYPE
    if not set to ANY)
    with arbitrary RDATA
  * the zone's CLASS value to indicate presence of a NAME,
    TYPE and given RDATA

We can distinguish these cases with query parameters, using
`req` for required requisites and `rej` for rejected
combinations.  The value for the parameter is a sequence
of words, separated by a space (query-escaped as a plus)
that respectively represent the NAME, the TYPE and a
series of RDATA fields.  The RDATA fields may be a group
of words when surrounded by brackets, like in a zone file.
Except for explicit word grouping, each RDATA field takes
the a form like in a DNS zone file, including the
[generic RDATA content notation](https://www.rfc-editor.org/rfc/rfc3597).

This means that you should always surround SRV or MX or SOA
arguments in brackets.  The reason becomes clear for TXT,
where brackets can capture multiple string fields in one RDATA,
while not using brackets would produce multiple RDATA.

Thanks to explicit brackets around word groups, multiple
RDATA values may be separated by spaces.  Empty RDATA follows
as zero such words after a space; no RDATA is specified as zero
such words with no prefixing space.

Using this, requirements can take the desired five forms:

`req=NAME+TYPE` for the value-independent existence of an RRset;
	there can be no (escaped) space after the TYPE, as that would
	be confused with the value-dependent form;

`req=NAME+TYPE+[RDATA...]` for the value-dependent existence of an RRset,
	where the `[RDATA...]` is a list of zero or more RDATA values,
	each as a hexadecimal word and separated by a space (that would
	be escaped as `+` or `%20`).  Note how the presence of a
	(possibly empty) RDATA list starts with a mandatory space
	(in `+` or `%20` escaped form) to distinguish it from the
	value-independent form;

`rej=NAME+TYPE` for the non-existence of an RRset;

`req=NAME` for the existence of a name; there can be no (escaped) space
	after the NAME, as that would be confused with the other forms;

`rej=NAME` for the non-existence of a name; there can be no (escaped) space
	after the NAME, as that would be confused with the other form.

These messages use a NAME set to a DNS owner name in lowercase form, without
trailing dot.  The TYPE follows the IANA-standardised uppercase form.


## Updates

The updates represet addition and (several levels of) deletion.
The CLASS may be ANY or NONE for removal or the zone's CLASS
for addition.  TTL is 0 on removal, and TYPE may be ANY.  These
distinctions may be expressed fully in query parameters.

```
CLASS    TYPE     RDATA    Meaning
---------------------------------------------------------
ANY      ANY      empty    Delete all RRsets from a name
ANY      rrset    empty    Delete an RRset
NONE     rrset    rr       Delete an RR from an RRset
zone     rrset    rr       Add to an RRset
```

Updates in Dynamic DNS are processed in their order of appearance,
and the same may be done with query parameters, as these are
strictly a string to be parsed in HTTP URIs.  The order of the
query string is the same as the order in the DNS update.

Updates can take the desired four forms:

`add=NAME+TTL+TYPE+RDATA` adds to an RRset.
	The CLASS is taken from the zone's CLASS.
	The format adds only one RDATA value.
	RDATA takes the same form as in the requirements and may
	be empty, but the trailing space (escaped as `+` or `%20`
	in the query string) is needed for consistency.

`del=NAME+TYPE` deletes an RRset.
	The CLASS is sent as ANY, TTL is 0 and RDATA is empty.

`del=NAME` deletes all RRsets for the given NAME.
	The CLASS is sent as ANY, TYPE is ANY, TTL is 0 and RDATA is empty.

`del=NAME+TYPE+RDATA` deletes one RR from an RRset.
	The CLASS is sent as NONE and TTL is 0.
	RDATA takes the same form as in the requirements and may
	be empty, but the trailing space (escaped as `+` or `%20`
	in the query string) is needed to avoid confusion with
	the `del=NAME+TYPE` query parameter.

These messages use a NAME set to a DNS owner name in lowercase form, without
trailing dot.  The TYPE follows the IANA-standardised uppercase form.


### Additional Data

Any additional data can be provided as `extra=NAME+TYPE+TTL+RDATA`
and will be sent with CLASS set to the zone's CLASS.  This data may
be out-of-zone data, which the receiving DNS server may choose to
honour or not.  Because of that, the NAME must be a fully qualified
domain name, which will be provided in lowercase and without trailing
dot.


### Local Extensions

When a record requires data but is handed empty data, or perhaps when 
fields have illegal data, they may be extended.  A few useful places
where this is often handy are:

  * When no RDATA is supplied for an A or AAAA record, the client's
    IPv4 or IPv6 address may be used.  If the TYPE differs from the
    client address, there would be an error.

  * When an illegal port number 0 is supplied in an SRV record,
    then the client's port may be used.  (Note that would be a stretch,
    but not undefensible to make this work along TCP, UDP and SCTP.)
    This facility can be used for the server variant on what STUN does
    for clients; this may be very helpful for publishing endpoints for
    peer-to-peer traffic, especially after assuring that a port is open
    and will remain open and look the same to all (which is trivial
    under IPv6 but is rather more troublesome under IPv4).


## Responses

The HTTP endpoint returns HTTP error codes to represent success or
failure.  Since Dynamic DNS Updates are executed completely or not
at all, such a single response code suffices.

The RCODE field in the DNS response is mapped to an HTTP error code:

  * `NOERROR` returns `200 Ok`
  * `FORMERR` returns `400 Bad Request`
  * `SERVFAIL` returns `500 Internal Server Error`
  * `NXDOMAIN` returns `410 Gone`
  * `NOTIMP` returns `501 Not Implemented`
  * `REFUSED` returns `403 Forbidden`
  * `YXDOMAIN` returns `412 Precondition Failed`
  * `YXRRSET` returns `412 Precondition Failed`
  * `NXRRSET` returns `412 Precondition Failed`
  * `NOTAUTH` returns `403 Forbidden`
  * `NOTZONE` returns `410 Gone`
  * Timeout returns `502 Gateway Error`

The body of the response is a JSON object, with a field `rcode`
set to the name string of the DNS error, or `SERVFAIL` in case of
a timeout.


## Access Control

To grant or withhold access to DNS record updates, we authenticate
with HTTP-SASL and subsequently look in an ACL to learn if the
authenticated user is granted the requested privilege.  The structures
for ARPA2 Access Control are worked out herein, by way of example.

Since the authenticated user is of the general form `user@domain.name`
it is possible to allow anyone.  The ACL is queried for the first match
in the sequence `user@domain.name`, `@domain.name` and `@.` where the
latter is the most generic form, namely a catch-all.

The entries are defined by a UUID 1b6abbb8-d580-3cbf-a6c6-4000095af382
for the `zonedata.uuid.arpa2.org` Access Type that controls who may
modify what parts of DNS.
The ZONE is used in the position of the Access Domain, to avoid
potential control differentiation caused by domain overlap and
to thwart attempts to escape the confines of a zone.
The Access Name consists of space-separated words:

  * The NAME which is a fully qualified DNS owner name, in lowercase and
    without trailing dot.  It is not advised to support the wildcard
    NAME form `*` in Access Control, but it may alternatively be
    useful to grant general access to any NAME and/or any TYPE
    underneath the zone.
  * The CLASS in capitals; usually `IN`.
  * The TYPE that may be changed, represented in uppercase; it is
    set to `ANY` to allow any TYPE for the given NAME.

To query if `AAAA` may be modified for `lab.ssh` under `example.com`,
the following Access Names are tried in order:

  * `lab.ssh.example.com IN AAAA` for access to the given NAME and TYPE;
  * `lab.ssh.example.com IN ANY` for access to the given NAME and any TYPE;

The following Access Rights must be found in the first match to
grant Dynamic DNS Updates:

  * `%A` or `ACCESS_ADMIN` sets the right to modify Access Rights;
  * `%S` or `ACCESS_SERVICE` is the highest automation privilege;
  * `%F` or `ACCESS_CONFIG` allows configuration (meaning?);
  * `%T` or `ACCESS_OPERATE` allows starting and stopping zone service;
  * `%D` or `ACCESS_DELETE` allows the removal of the respective NAME or RRset
	(which gives rise to an existence precondition if not also marked
	with `%W`);
  * `%O` or `ACCESS_OWNER` allows the creation of a new NAME under a
	non-existence precondition as an owner; **TODO:** Note the ownership
	somewhere... updates to the ACL are security-sensitive though;
	also remove when the name is deleted.
  * `%C` or `ACCESS_CREATE` allows the creation of a new (NAME and) RRset
	(which gives rise to a non-existence precondition if not also
	marked with `%W`);
  * `%W` or `ACCESS_WRITE` allows the modification of an existing RRset
	other than creating or deleting it (which gives rise to an
	non-existence or existence precondition respectively if not also
	marked with `%C` or `%D` respectively);

The idea of these flags is that `%CW` allows changes with creation on the fly,
`%DW` allows changes with deletion on the fly and `%CDW` allows any desired change.
**TODO:** Maybe also require at least one of `%OAS` to grant any changes?

Before these flags, it is possible to set something like `i=300` as the
minimum TTL, or `x=608400` as the maximum TTL.  When not presented, these
are the values that are used.  Variables set after the %FLAGS will not be
noticed.

Proper distribution of these flags supports many useful DNS operational uses:

  * Administrators may add and remove hosts and services by hand;
  * Child zones may have their own change policy in the parent zone;
  * Bots may modify TLSA records during service certificate updates;
  * Plugin services may rewrite names allocated to them;
  * Users may define a fixed DNS name to capture dynamic IP addresses;
  * Phone users may setup ENUM records under their phone number,
    presumably using suitable tools, and possibly delegate the
    management to an internet or telecom operator hosting operator.

The use of SASL makes this as secure as a site wants it to be.
This is an improvement of the flexibility of other proprietary
web-protocols for Dynamic DNS over HTTP.

