# ARPA2 Dynamic DNS Gateway for HTTP

> *This is a gateway from HTTP to standardised Dynamic DNS.
> It allows many more forms than just setting IP addresses.
> It uses ARPA2 Access Control after HTTP-SASL authentication.*

This gateway can be useful in many places:

  * To setup a dynamic IP address for a fixed hostname published in DNS
  * To allow servers, even by foreign parties, to udpate DNS settings
    such as IP addresses, SRV records, and DANE certificates
  * To modify ENUM settings for a phone number, and possibly even SRV
    records pointing to a general domain service under that number

These facilities are usually built into DNS systems, and difficult to
reach.  There are good (security) reasons for not making those widely
available.

The ARPA2 tools allow a suitable level of control to allow the release
of these facilities over something as friendly as HTTP; this allows us
to use HTTP-SASL, including its option for Realm Crossover, and there
is a well-defined Access Control mechanism that enables fine control
over who may change what DNS records, up to individually owned records.


## Building the Gateway

The same build scheme as for other ARPA2 projects applies:

```
mkdir build
cd build
cmake ..
make
make install
```

The following ARPA2 projects are required dependencies:

  * [ARPA2CM](https://gitlab.com/arpa2/arpa2common)
  * [ARPA2Common](https://gitlab.com/arpa2/arpa2common)

The following libraries are commonly distributed in operating systems:

  * FastCGI development libraries, `libfcgi-dev`
  * LDNS library from NLnet Labs, `libldns-dev`
  * LMDB database library, `liblmdb-dev` (FIXME: indirect dependency)
  * COM_ERR library, `comerr-dev` (FIXME: indirect dependency)

This version assumes that HTTP-SASL (or other) authentication is
handled in the web frontend.


## Using the Gateway

This is a FastCGI plugin for a web server.  Configure the web server
to achieve client authentication before passing on the request to this
gateway.  The authenticated identity should be passsed in the
`REMOTE_USER` CGI variable.  Also pass `ARPA2_DOMAIN` with the domain
name being used, and `ARPA2_SERVICEKEY` with the domain-specific
Service Key for lookups in the Rules DB.
Finally, the `QUERY_STRING` CGI variable is used to
get hold of the actual Update to relay to DNS.

Clients may now invoke the script using the GET method and a set of
`?` parameters as per the gateway specification.  Clients need to
accommodate the HTTP-SASL exchange to authenticate the client, and
will then try to authorise the requested changes for that client.

The gateway may report back HTTP error codes if something did not
work as intended.  It returns `200 Ok` only when all went well,
including its back-call to the DNS server.  The body of the response
is always a JSON object, with `rcode` set to a string for the
reported condition, for example

```
{
   "rcode": "SERVFAIL",
   ...
}
```


