/* http2dnsupdate.c -- Gateway from HTTP to DNS UPDATE requests
 *
 * Dynamic DNS is a useful standard, but since it uses a DNS protocol, it
 * is not able to perform authentication and authorisation well enough to
 * grant general access.  Proprietary solutions have been built for updates
 * to (mostly) AAAA and A records, but not general DNS records.
 *
 * This HTTP gateway wraps around Dynamic DNS, including preconditions,
 * and assumes that HTTP-SASL has setup a valid REMOTE_USER which is then
 * used herein to run the desired changes through ARPA2 Access Control.
 *
 * The result is that this gateway can be published to anyone, granting
 * even rights to foreign users (if they were let in) to records that they
 * might desire to change.
 *
 * This programs links the following elements:
 *  - FastCGI after a web (or CoAP) frontend with HTTP-SASL authentication
 *  - ARPA2 Access Control to grant and retract permissions on DNS records
 *  - Dynamic DNS as in RFC 2136
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdbool.h>

#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <fcgi_stdio.h>		/* TODO: OR MUST THIS BE ON TOP? */

#include <ldns/ldns.h>

#undef  LOG_STYLE
#define LOG_STYLE LOG_STYLE_SYSLOG

#include <arpa2/identity.h>
#include <arpa2/access.h>
#include <arpa2/rules.h>
#include <arpa2/except.h>



/* The type that collects query data, both from reference and
 * the LDNS objects being collected towards a DNS packet.
 */
enum {
	ACCU_LIST_PREREQ,
	ACCU_LIST_UPDATES,
	ACCU_LIST_ADDITIONAL,
	ACCU_LIST_SIZE
};
//
typedef struct {
	//
	// The zone name to work on
	const char *zone;
	//
	// The authenticated remote user
	a2id_t a2id_user;
	//
	// The service key for the zone
	const uint8_t *svckey;
	uint16_t svckeylen;
	//
	// The CLASS to work in
	const char *class;
	ldns_rr_class cls;
	//
	// Collecting the RR lists for the DNS UPDATE packet
	ldns_rr_list *lists [ACCU_LIST_SIZE];
} reqdata_t;


/* The request that passes into rules_dbiter() and callbacks.
 * Also learn the min/max TTL values when i=.../x=... precede %FLAGS
 */
struct dnsupdate_request {
	struct rules_request base;
	access_rights collected_rights;
	uint32_t minttl;
	uint32_t maxttl;
};



/* The Access Type is the UUID 1b6abbb8-d580-3cbf-a6c6-4000095af382
 * for zonedata.uuid.arpa2.org.
 */
access_type access_type_zonedata = {
	0x1b, 0x6a, 0xbb, 0xb8, 0xd5, 0x80, 0x3c, 0xbf,
	0xa6, 0xc6, 0x40, 0x00, 0x09, 0x5a, 0xf3, 0x82,
};



/* Get a FastCGI variable, clearing "ok" if it is required-yet-missing.
 */
char *getcgivar (char *name, bool required, bool *ok) {
	char *retval = getenv (name);
	if (retval == NULL) {
		log_error ("Missing FastCGI variable %s", name);
		if (required) {
			*ok = false;
		}
	}
	return retval;
}


/* Map one range of characters to another; make_uppercase() and make_lowercase().
 * Also, make_zone() and make_kwd().
 */
void make_different (char *str, char ins, char ine, char outs) {
	if (str == NULL) {
		return;
	}
	outs -= ins;
	while (*str != '\0') {
		if ((*str >= ins) && (*str <= ine)) {
			*str += outs;
		}
		str++;
	}
}
//
static inline void make_uppercase (char *str) {
	make_different (str, 'a', 'z', 'A');
}
//
static inline void make_lowercase (char *str) {
	make_different (str, 'A', 'Z', 'a');
}
//
static inline void make_zone (char *str) {
	make_lowercase (str);
	//TODO// Drop trailing dot
}
//
static inline void make_kwd (char *str) {
	make_uppercase (str);
}


/* Parse a TTL string and return it as a uint32_t value.
 * Apply reasonable boundaries.  Return 0 on parse error.
 */
uint32_t parse_ttl (char *ttls, uint32_t min_ttl, uint32_t max_ttl) {
	char *ttle = NULL;
	unsigned long ttll;
	ttll = strtoul (ttls, &ttle, 10);
	if ((*ttle != '\0') || (ttll == 0) || (ttll > 0xffffffff)) {
		return 0;
	}
	uint32_t ttl;
	if (ttll < min_ttl) {
		ttl = min_ttl;
	} else if (ttll > max_ttl) {
		ttl = max_ttl;
	} else {
		ttl = ttll;
	}
	return ttl;
}


/* Collect the Access Rights in rules_dbiter() callbacks for a dnsupdate_request.
 * When i=... precedes the %FLAGS, possibly increase minttl accordingly.
 * When x=... precedes the %FLAGS, possibly decrease maxttl accordingly.
 */
static bool collect_rights (struct rules_request *rreq) {
	log_info ("Got access rights 0x%08lx or %s\n", rreq->flags, rreq->flags_rawstr);
	struct dnsupdate_request *dreq = (struct dnsupdate_request *) rreq;
	dreq->collected_rights |= rreq->flags;
	char *endptr;
	unsigned long ttlval;
	if (rreq->varray_strlen ['i'-'a'] > 0) {
		ttlval = strtoul (rreq->varray_str ['i'-'a'], &endptr, 10);
		if (endptr == rreq->varray_str ['i'-'a'] + rreq->varray_strlen ['i'-'a']) {
			if ((ttlval > dreq->minttl) && (ttlval <= 0xffffffff)) {
				dreq->minttl = (uint32_t) ttlval;
			}
		}
	}
	if (rreq->varray_strlen ['x'-'a'] > 0) {
		ttlval = strtoul (rreq->varray_str ['x'-'a'], &endptr, 10);
		if (endptr == rreq->varray_str ['x'-'a'] + rreq->varray_strlen ['x'-'a']) {
			if ((ttlval > dreq->maxttl) && (ttlval <= 0xffffffff)) {
				dreq->maxttl = (uint32_t) ttlval;
			}
		}
	}
	return true;
}


/* Determine Access Rights, returning true when something is found.
 * The Access Name is formatted from xsnamef and additional parameters.
 * Also update any values for minttl and maxttl, if defined.
 */
bool determine_rights (reqdata_t *accu, access_rights *outw, uint32_t *minttl, uint32_t *maxttl, const char *xsnamef, ...) {
	//
	// Produce the Access Name
	char xsname [520];
	va_list ap;
	va_start (ap, xsnamef);
	int sz = vsnprintf (xsname, 515, xsnamef, ap);
	va_end (ap);
	if (sz > 512) {
		return false;
	}
	//
	// Construct the request
	struct dnsupdate_request dreq;
	memset (&dreq, 0, sizeof (dreq));
	dreq.base.flags = ACCESS_VISITOR;
	dreq.base.optcb_endrule = collect_rights;
	dreq.minttl = 0;
	dreq.maxttl = *maxttl;
	//
	// Iterate the database
	bool ok = rules_dbiterate (&dreq.base, accu->svckey, accu->svckeylen, xsname, &accu->a2id_user);
	if (!ok) {
		log_errno ("Error from rules_dbiterate()");
	} else {
		log_debug ("Return 0x%08lx from rules_dbiterate()", dreq.base.flags);
	}
	log_debug ("After rules_dbiterate() collected_rights = 0x%08lx\n", dreq.collected_rights);
	//
	// Practical adaptions to minttl and maxttl after ruleset processing
	if (dreq.minttl < *minttl) {
		dreq.minttl = *minttl;
	}
	if (dreq.maxttl < dreq.minttl) {
		dreq.maxttl = dreq.minttl;
	}
	//
	// Return the result -- be it with or without ACCESS_WRITE
	// The flag indicates whether we found a (non-empty) flags string
	*outw = dreq.collected_rights;
	*minttl = dreq.minttl;
	*maxttl = dreq.maxttl;
	return ok;
}


/* Parse a hex digit to a value, return > 0xff on error.
 * If no error, advance the query string pointer.
 */
unsigned hex2value (char **pqstr) {
	char ch = **pqstr;
	if ((ch >= '0') && (ch <= '9')) {
		ch -= '0';
	} else {
		ch &= 0xdf;
		if ((ch >= 'A') && (ch <= 'F')) {
			ch -= 'A' - 10;
		} else {
			return 0xffff;
		}
	}
	(*pqstr)++;
	return ch;
}


/* Return the first of a (sequence of) words or, if none remain,
 * return the fallback which can be a string or NULL.
 *
 * Originally, words are separated by spaces and by the time this
 * is called they have been separated by NUL characters.  Using
 * brackets, it is possible to group words and retain a space in
 * a field, notably in the RDATA part, where the nextfield()
 * function can be used to further split the RDATA word.
 *
 * If a word is returned, then move the word counter to the next word
 * and lower the remaining word count.
 */
char *nextword (char **pwords, unsigned *pwordc, char *fallback) {
	char *retval = fallback;
	if (*pwordc > 0) {
		retval = *pwords;
		*pwords = retval + strlen (retval) + 1;
		(*pwordc)--;
	}
	return retval;
}


/* Within a word, return the next space-separated field.
 * This is used to iterate over RDATA fields of an RR.
 * The *pfields value is initially the start of the word
 * holding the start of the RDATA word; since multiple
 * RDATA words are possible, the two levels must be kept
 * separate, and nextword() can be called to retrieve
 * the next RDATA word.
 *
 * Strings surrounded by double quotes are treated in a
 * special way, as separate words.  TODO: No escapes yet.
 *
 * When no more words are available, *pfields points to
 * the original NUL.  When more is available, *pfields
 * points to the next field to return.  This helps to
 * return strings (with space replaced by NUL) and a
 * final NULL to indicate that there are no more fields.
 * This form works for fixed-form RDATA such as MX and
 * variable-sized RDATA such as TXT.
 */
char *nextfield (char **pfields) {
	char *retval = *pfields;
	if (*retval == '\0') {
		return NULL;
	}
	char *next = retval;
	while ((*next != ' ') && (*next != '\0')) {
		if (*next == '"') do {
			next++;
		} while ((*next != '"') && (*next != '\0'));
		if (*next != '\0') {
			next++;
		}
	}
	if (*next == ' ') {
		*next++ = '\0';
	}
	*pfields = next;
	return retval;
}


/*
 * Following is some LDNS logic.  It is loosely based on the example in
 * https://github.com/NLnetLabs/ldns/blob/develop/examples/ldns-update.c
 * and, where documentation is short, reading of the API implementation.
 *
 */



/* Add an RR to an RRlist.
 *
 * Very, very free form to allow all DNS UPDATE idiosyncracies.
 *
 * rdatac>0 => rdatas is a concatenation of that many NUL-terminated strings.
 */
bool have_rr (reqdata_t *accu, int rrlistno, char *fqdn, uint16_t ttl, uint16_t cls, const char *type, char *rdatas, unsigned rdatac) {
	//
	// Create the desired RR, with desired TTL, CLASS and RRTYPE
	ldns_rr *vroarr = ldns_rr_new ();
	ldns_rr_set_owner    (vroarr, ldns_dname_new_frm_str (fqdn));
	ldns_rr_set_ttl      (vroarr, ttl);
	ldns_rr_set_class    (vroarr, cls);
	ldns_rr_type typ = ldns_get_rr_type_by_name (type);
	ldns_rr_set_type     (vroarr, typ);
	//
	// Check whether to proceed
	if (typ == 0) {
		goto fail_free;
	}
	//
	// Iterate over the RDATA words and push them into the RR
	// but only add RDATA words when they are given
	for (int rdatano = 0; rdatano < rdatac; rdatano++) {
		char *rdfs = nextword (&rdatas, &rdatac, NULL);
		//
		// Consider that the RDATA may be unknown \# data from RFC 3597
		if (strncmp (rdfs, "\\#", 2) == 0) {
			//
			// Add the whole RDATA field as one word
			ldns_rdf *vroemm = ldns_rdf_new_frm_str (LDNS_RDF_TYPE_UNKNOWN, rdfs);
			if (vroemm == NULL) {
				goto fail_free;
			}
			ldns_rr_push_rdf (vroarr, vroemm);
			//
			// Special case; skip normal loop handling
			continue;
		}
		//
		// Iterate over RDF fields within the RDATA word; words may span
		// multiple fields when those fields are surrounded by brackets;
		// Words change spaces to NUL only outside of brackets (and the
		// brackets are removed).  Then, the fields may change spaces to
		// NUL in the following inner loop
		const ldns_rr_descriptor *desc = ldns_rr_descript (typ);
		unsigned rdfidx = 0;
		char *rdf;
		while (rdf = nextfield (&rdfs), rdf != NULL) {
			//
			// Determine the RDATA field type from the RR descriptor
			ldns_rdf_type rdftyp = ldns_rr_descriptor_field_type (desc, rdfidx++);
			//
			// Produce the RDATA field and add it to the RR
			ldns_rdf *vroemm = ldns_rdf_new_frm_str (rdftyp, rdf);
			if (vroemm == NULL) {
				goto fail_free;
			}
			ldns_rr_push_rdf (vroarr, vroemm);
		}
	}
	//
	// Be sure to have the desired RRlist
	ldns_rr_list *list = accu->lists [rrlistno];
	if (list == NULL) {
		accu->lists [rrlistno] = list = ldns_rr_list_new ();
		if (list == NULL) {
			goto fail_free;
		}
	}
	//
	// Add the new RR to the RRlist
	ldns_rr_list_push_rr (list, vroarr);
	return true;
	//
	// Failure, free the RR
fail_free:
	ldns_rr_free (vroarr);
fail:
	return false;
}


/* Argument callback for "zclass=...".
 */
bool arg_zclass (reqdata_t *accu, bool _, char *words, unsigned wordc) {
	//
	// Validate the number of argument words
	if (wordc != 1) {
		return false;
	}
	//
	// Set the zone class
	ldns_rr_class cls = ldns_get_rr_class_by_name (words);
	if (cls == 0) {
		return false;
	} else if (cls != LDNS_RR_CLASS_IN) {
		log_error ("Attempt to use zclass=%s (not IN) which may be insecure", words);
		return false;
	} else {
		accu->class = words;
		accu->cls = cls;
		return true;
	}
}


/* Argument callback for "zone=...".
 */
bool arg_zone (reqdata_t *accu, bool _, char *words, unsigned wordc) {
	//
	// Validate the number of argument words
	if (wordc != 1) {
		return false;
	}
	//
	// Only compare if the zone was already set
	if (accu->zone != NULL) {
		return (strcasecmp (accu->zone, words) == 0);
	}
	//
	// Set the zone if it is newly provided
	make_zone (words);
	accu->zone = words;
	return true;
}


/* Argument callback for "req=..." and "rej=...".
 *
 * CLASS    TYPE     RDATA    Meaning
 * ------------------------------------------------------------
 * ANY      ANY      empty    Name is in use
 * ANY      rrset    empty    RRset exists (value independent)
 * NONE     ANY      empty    Name is not in use
 * NONE     rrset    empty    RRset does not exist
 * zone     rrset    rr       RRset exists (value dependent)
 *
 * Their QUERY_STRING forms are, respectively:
 *
 * req=NAME
 * req=NAME+TYPE
 * rej=NAME
 * rej=NAME+TYPE
 * req=NAME+TYPE+[RDATA...]
 */
bool arg_req_rej (reqdata_t *accu, bool req_not_rej, char *words, unsigned wordc) {
	//
	// Validate the number of argument words
	if ((wordc < 1) || ((wordc > 2) && !req_not_rej)) {
		return false;
	}
	//
	// Fetch the NAME and TYPE -- remains are RDATA...
	char *name = nextword (&words, &wordc, NULL);
	char *type = nextword (&words, &wordc, NULL);
	if (type == NULL) {
		type = "ANY";
	}
	make_kwd (type);
	if (wordc == 0) {
		words = NULL;
	}
	//
	// Derive CLASS and TYPE for the RR
	ldns_rr_class cls = LDNS_RR_CLASS_NONE;
	if (req_not_rej) {
		cls = (wordc > 0) ? accu->cls : LDNS_RR_CLASS_ANY;
	}
	//
	// All variants set TTL to 0
	uint32_t ttl = 0;
	//
	// Have the RR in the DNS UPDATE
	return have_rr (accu, ACCU_LIST_PREREQ, name, ttl, cls, type, words, wordc);
}


/* Argument callback for "add=..." and "del=...".
 *
 * CLASS    TYPE     RDATA    Meaning
 * ---------------------------------------------------------
 * ANY      ANY      empty    Delete all RRsets from a name
 * ANY      rrset    empty    Delete an RRset
 * NONE     rrset    rr       Delete an RR from an RRset
 * zone     rrset    rr       Add to an RRset
 *
 * Their QUERY_STRING forms are, respectively:
 *
 * del=NAME
 * del=NAME+TYPE
 * del=NAME+TYPE+RDATA
 * add=NAME+TTL+TYPE+RDATA
 */
bool arg_add_del (reqdata_t *accu, bool add_not_del, char *words, unsigned wordc) {
	//
	// Validate the number of argument words
	if (add_not_del) {
		if (wordc != 4) {
			return false;
		}
	} else {
		if ((wordc < 1) || (wordc > 3)) {
			return false;
		}
	}
	//
	// Fetch the NAME, if present
	char *name = nextword (&words, &wordc, NULL);
	//
	// Fetch the TTL, if this is an add=
	// Defer processing until minttl and maxttl are known
	char *ttls = add_not_del ? nextword (&words, &wordc, NULL) : "0";
	//
	// Fetch the TYPE, if present
	char *type = nextword (&words, &wordc, NULL);
	if (type == NULL) {
		type = "ANY";
	}
	make_kwd (type);
	//
	// Derive the CLASS value
	ldns_rr_class cls;
	const char *class;
	if (add_not_del) {
		cls   = accu->cls;
		class = accu->class;
	} else if (wordc == 0) {
		cls   = LDNS_RR_CLASS_ANY;
		class = "ANY";
	} else {
		cls   = LDNS_RR_CLASS_NONE;
		class = "NONE";
	}
	//
	//
	// Determine Access Rights for the queried name for this user;
	// require ACCESS_WRITE, and fetch ACCESS_CREATE and ACCESS_DELETE
	uint32_t rights = ACCESS_VISITOR;
	uint32_t minttl = 300;
	uint32_t maxttl = 604800;
	bool gotxs = false;
	if (!gotxs) {
		//TODO// Maybe not always have a type
		gotxs = determine_rights (accu, &rights, &minttl, &maxttl, "%s %s %s",  name, class, type);
	}
	if (!gotxs) {
		gotxs = determine_rights (accu, &rights, &minttl, &maxttl, "%s %s ANY", name, class);
	}
	if (0 == (rights & ACCESS_WRITE)) {
		log_info ("Unauthorised DNS UPDATE attempt by %s on zone %s: %s %s %s|ANY",
				accu->a2id_user.txt, accu->zone,
				name, class, type);
		return true;
	}
	//TODO// Require pre-existence if...
	//TODO// Require pre-absense if...
	//
	// Now parse the TTL, taking minttl and maxttl into account
	uint32_t ttl = add_not_del ? parse_ttl (ttls, minttl, maxttl) : 0;
	//
	// If present, the RDATA is in words and wordc
	if (wordc == 0) {
		words = NULL;
	}
	//
	// Have the RR in the DNS UPDATE
	return have_rr (accu, ACCU_LIST_UPDATES, name, ttl, cls, type, words, wordc);
}


/* Argument callback for "extra=...".
 *
 * The QUERY_STRING form is:
 *
 * extra=NAME+TYPE+TTL+RDATA...
 */
bool arg_extra (reqdata_t *accu, bool _, char *words, unsigned wordc) {
	//
	// Validate the number of argument words
	if (wordc < 4) {
		return false;
	}
	//
	// Fetch the NAME, TYPE and TTL
	char *name = nextword (&words, &wordc, NULL);
	char *type = nextword (&words, &wordc, NULL);
	make_kwd (type);
	char *ttls = nextword (&words, &wordc, NULL);
	uint32_t ttl = parse_ttl (ttls, 300, 604800);
	if (ttl == 0) {
		return false;
	}
	//
	// Compute and check the TTL value
	//
	// The RDATA is in words and wordc
	;
	//
	// Have the RR in the DNS UPDATE
	return have_rr (accu, ACCU_LIST_ADDITIONAL, name, ttl, accu->cls, type, words, wordc);
}


/* Collect arguments.  Multiple entries may occur, in a meaningful
 * order.  Different arguments are provided in a standard order.
 *
 * Two names may be provided, to occur in arbitrary order.  It is
 * assumed that the sequence runs to completion and is then done,
 * as a result of the predetermined QUERY_STRING argument order.
 * If only one name is to be recognised, simply provide it twice.
 *
 * When an argument of a matching name is found, its format is
 * rewritten in-place (which only reduces the size) and the parser
 * pointer advanced beyond the argument and any '&' connector,
 * which may also be rewritten.
 *
 * The callback function is invoked with the value of the argument,
 * along with the reqdata_t structure that acts as an accumulator,
 * and which holds an LDNS structure being built up.
 *
 * This function returns false in case of error, with min/max
 * match counters that are permitted.
 *
 * After a sequence of calls to this function returns true, the
 * full string is parsed if (and only if) the parser pointer is
 * a pointer to the NUL byte after the query string.
 */
bool collect_argument (char **pqstr, reqdata_t *accu,
			bool callback (reqdata_t *accu, bool arg0, char *words, unsigned wordc),
			const char *argname1, const char *argname2, int argnamelen,
			unsigned min_match, unsigned max_match) {
	//
	// Copy the parse string locally
	char *qstr = *pqstr;
	//
	// Match sequences of the next keyword
	unsigned matches = 0;
	bool match1, match2;
	while (match1 = (memcmp (qstr, argname1, argnamelen) == 0),
	       match2 = (memcmp (qstr, argname2, argnamelen) == 0),
	       (match1 || match2) && (qstr [argnamelen] == '=')) {
		//
		// Count the match (as any parsing error returns false)
		matches++;
		//
		// Setup pointers to read and write the parameter
		// so we can copy/reduce qstr as a literal string
		char *qrd = qstr;
		char *qwr = qstr;
		//
		// Move beyond the keyword and '=' sign
		qstr += argnamelen + 1;
		//
		// Copy the argument, de-esscaping "+" and "%xx"
		// where "+" and "%20" end words except within (...)
		bool more = true;
		unsigned wordc = 1;
		int bra_ket = 0;
		int quoted = 0x0000;
		while (true) {
			unsigned ch;
			switch (*qstr | quoted) {
			case '\0' | 0x1000:
			case '\0':
				more = false;
				/* continue as for '&'... */
			case '&':
				goto endarg;
			case '"' ^ 0x1000:
			case '"':
				quoted ^= 0x1000;
				*qwr++ = *qstr++;
				break;
			case '(':
				bra_ket++;
				qstr++;
				break;
			case ')':
				bra_ket--;
				qstr++;
				break;
			case '%':
				qstr++;
				ch = hex2value (&qstr) << 4;
				ch = hex2value (&qstr) | ch;
				if (ch > 0xff) {
					goto error_skip;
				} else if (ch != ' ') {
					*qwr++ = ch;
					break;
				}
				/* For "%20" continue into the "+" case... */
			case '+':
				if (bra_ket == 0) {
					//
					// Separate RDATA fields
					*qwr++ = '\0';
					wordc++;
				} else {
					//
					// Separate words within an RDATA field
					*qwr++ = ' ';
				}
				qstr++;
				break;
			default:
				*qwr++ = *qstr++;
				break;
			}
		}
endarg:
		//
		// If more, skip beyond '&'; write trailing '\0'
		*qwr = '\0';
		if (more) {
			qstr++;
		}
		//
		// Invoke the callback, flagging first-not-second match
		bool ok = callback (accu, match1, qrd, wordc);
		if (!ok) {
			goto error;
		}
		//
		// Continue to the next argument (with an optimisation)
		if (!more) {
			break;
		}
	}
	//
	// Save the local copy of the query string pointer
	*pqstr = qstr;
	//
	// Conclude if we matched well enough
	return ((matches >= min_match) && (matches <= max_match));
	//
	// An error occurred while processing, so return false
error_skip:
	//
	// Possibly skip beyond an erreneous argument
	if (*qstr != '\0') {
		while ((*qstr != '&') && (*qstr != '\0')) {
			qstr++;
		}
	}
error:
	//
	// Save the local query string pointer and return failure
	*pqstr = qstr;
	return false;
}


/* Process a request.  Check the input and produce an error or
 * attempt to pass the DNS UPDATE request to the DNS backend.
 */
bool process_request (ldns_resolver *upstream) {
	bool ok =true;
	//
	// Prepare a request data structure to collect the DNS UPDATE
	reqdata_t accu;
	memset (&accu, 0, sizeof (accu));
	//
	// Retrieve envvars
	char *remote_user      = getcgivar ("REMOTE_USER",      true,  &ok);
	char *query_string     = getcgivar ("QUERY_STRING",     true,  &ok);
	char *arpa2_domain     = getcgivar ("ARPA2_DOMAIN",     false, &ok);
	char *arpa2_servicekey = getcgivar ("ARPA2_SERVICEKEY", true,  &ok);
	//
	// Check the Access Control is properly provided
	if (ok && (arpa2_domain == NULL) && (arpa2_servicekey != NULL)) {
		log_error ("Cannot use ARPA2_SERVICEKEY without ARPA2_DOMAIN");
		ok = false;
	}
	//
	// Parse the REMOTE_USER as an ARPA2 Remote Identity
	if (ok && ((remote_user == NULL) || !a2id_parse_remote (&accu.a2id_user, remote_user, 0))) {
		log_errno ("Failed to parse REMOTE_USER \"%s\" as ARPA2 Remote Identity", remote_user);
		ok = false;
	}
	//
	// Fill the request data structure to collect the DNS UPDATE
	make_zone (arpa2_domain);
	accu.zone = arpa2_domain;
	accu.cls   = LDNS_RR_CLASS_IN;
	accu.class = "IN";
	//accu.remote_user = remote_user;
	accu.svckeylen = strlen (arpa2_servicekey);
	bool goodkey = ((accu.svckeylen & 0x0001) == 0x0000);
	accu.svckeylen >>= 1;
	uint8_t svckeymem [accu.svckeylen];
	accu.svckeylen = 0;
	while (goodkey && (*arpa2_servicekey != '\0')) {
		unsigned ch;
		ch = hex2value (&arpa2_servicekey) << 4;
		ch = hex2value (&arpa2_servicekey) | ch;
		goodkey = (ch <= 0xff);
		svckeymem [accu.svckeylen++] = (uint8_t) ch;
	}
	if (!goodkey) {
		log_error ("Syntax or length error in ARPA2_SERVICEKEY");
		ok = false;
	}
	accu.svckey = svckeymem;
	//
	// Parse the QUERY_STRING while de-escaping it in-place
	bool stx = ok;
	stx = stx && collect_argument (&query_string, &accu, arg_zclass,  "zclass", "zclass", 6, 0,  1);
	stx = stx && collect_argument (&query_string, &accu, arg_zone,    "zone",   "zone",   4, 0,  1);
	stx = stx && collect_argument (&query_string, &accu, arg_req_rej, "req",    "rej",    3, 0, ~0);
	stx = stx && collect_argument (&query_string, &accu, arg_add_del, "add",    "del",    3, 1, ~0);
	stx = stx && collect_argument (&query_string, &accu, arg_extra,   "extra",  "extra",  5, 0, ~0);
	stx = stx && (*query_string == '\0');
	stx = stx && (accu.zone != NULL);
	if (ok && !stx) {
		log_error ("Invalid syntax for QUERY_STRING");
		ok = false;
	}
	//
	// Form the DNS UPDATE request
	ldns_pkt *request = NULL;
	if (ok) {
		request = ldns_update_pkt_new (
					ldns_dname_new_frm_str (accu.zone),
					accu.cls,
					accu.lists [ACCU_LIST_PREREQ],
					accu.lists [ACCU_LIST_UPDATES],
					accu.lists [ACCU_LIST_ADDITIONAL]);
		ok = ok && (request != NULL);
		//
		// Cleanup any lists that we may have
		for (int idx = 0; idx < ACCU_LIST_SIZE; idx++) {
			ldns_rr_list_deep_free (accu.lists [idx]);
		}
		memset (accu.lists, 0, sizeof (accu.lists));
	}
	//
	// Add a TSIG to the DNS UPDATE
	if (ok) {
		ldns_status tsigres = ldns_update_pkt_tsig_add (request, upstream);
		if (tsigres != LDNS_STATUS_OK) {
			log_error ("Failed to add TSIG signature: %s",
					ldns_get_errorstr_by_id (tsigres));
			ok = false;
		}
	}
	//
	// Send the request to the DNS server and await the response
	ldns_pkt *response = NULL;
	if (ok) {
		if (LDNS_STATUS_OK == ldns_resolver_send_pkt (&response, upstream, request)) {
			ldns_pkt_rcode rcode = ldns_pkt_get_rcode (response);
			"TODO";
		}
	}
	//
	// Cleanup request and response packets, if any
	if (response != NULL) {
		ldns_pkt_free (response);
	}
	if (request != NULL) {
		ldns_pkt_free (request);
	}
	//
	// Return whether the request was a success
	return ok;
}



int main (int argc, char *argv []) {
	//
	// Parse the command line arguments
	const char *progname = *argv;
	bool ok = true;
	bool help = false;
	const char *tsigname = NULL;
	const char *tsigalg  = NULL;
	const char *tsigfile = NULL;
	const char *server = "::1";
	uint32_t port = 53;
	int opt;
	while ((opt = getopt (argc, argv, "a:n:k:s:p:h")) != -1) {
		switch (opt) {
		case 'a':
			tsigalg = optarg;
			break;
		case 'n':
			tsigname = optarg;
			break;
		case 'k':
			tsigfile = optarg;
			break;
		case 's':
			server = optarg;
			break;
		case 'p':
			port = parse_ttl (optarg, 0, 65536);
			log_critical ("DNS server port support not yet built in\n");
			ok = false;
			break;
		case 'h':
			help = true;
			break;
		default:
			log_critical ("Unknown option '%c'\n", opt);
			ok = false;
			help = true;
			break;
		}
	}
	//
	// Check correctness and completeness of the arguments
	if ((tsigalg == NULL) || (tsigname == NULL)) {
		log_critical ("Missing TSIG configuration: -a TSIGALG -n TSIGNAME [-k TSIGKEYFILE]\n");
		ok = false;
	}
	if ((port == 0) || (port > 65535)) {
		log_critical ("Port out of UDP range\n");
		ok = false;
	}
	//
	// Provide commandline help, and terminate after argument errors
	if (help) {
		log_critical ("%s is a HTTP proxy to DNS UPDATE requests, for Dynamic DNS\n"
			"[-h]             show this help text\n",
			"[-s SERVERADDR]  set the IPv6 address of the DNS server to update, default ::1\n"
		//	"[-p SERVERPORT]  set the UDP port for DNS UPDATE, default 53\n"
			"-a TSIGALG       set the TSIG algorithm name\n"
			"-n TSIGNAME      set the TSIG key name\n"
			"[-k TSIGKEYFILE] set the TSIG key file, default &3\n"
			,progname);
	}
	if (!ok) {
		exit (1);
	}
	//
	// Try to open &3 or if it is absent try the default key file
	char keydata [1030];
	int kf = 3;
	struct stat st;
	if (tsigfile != NULL) {
		kf = open (tsigfile, O_RDONLY);
	}
	if (kf >= 0) {
		ssize_t rdlen = read (kf, keydata, 1024);
		close (kf);
		if (rdlen == 0) {
			log_critical ("End of file while loading TSIG key\n");
			exit (1);
		} else if (rdlen < 0) {
			perror ("Error while loading TSIG key");
			exit (1);
		}
		keydata [rdlen] = '\0';
	}
	//
	// Setup the DNS upstream, that is where the DNS UPDATE goes to
	ldns_resolver *upstream = ldns_resolver_new ();
#if 0
//TODO// Silly to use the resolver from /etc/resolv.conf
	if (LDNS_STATUS_OK != ldns_resolver_new_frm_file (&upstream, NULL)) {
		log_critical ("Failed to initialise from /etc/resolver.conf");
	}
#endif
	ldns_rdf *nsrdf = ldns_rdf_new_frm_str (LDNS_RDF_TYPE_AAAA, server);
	if (nsrdf != NULL) {
		ldns_resolver_push_nameserver (upstream, nsrdf);
	} else {
		log_error ("Failed to parse nameserver %s as an IPv6 address", "::1");
	}
	//
	// Install TSIG configuration strings for the DNS UPDATE upstream
	ldns_resolver_set_tsig_keyname   (upstream, tsigname);
	ldns_resolver_set_tsig_algorithm (upstream, tsigalg );
	ldns_resolver_set_tsig_keydata   (upstream, keydata );
	//
	// Validate upstream DNS UPDATE server
	if (ldns_resolver_nameserver_count (upstream) == 0) {
		log_critical ("No nameserver was configured");
		exit (1);
	}
	//
	// Initialise the ARPA2 Access Control system
	access_init ();
	//
	// Fetch the/a request
	log_info ("Ready to process HTTP requests for DNS UPDATE");
	while (FCGI_Accept () >= 0) {
		if (!process_request (upstream)) {
			log_info ("Failed DNS UPDATE request");
			FCGI_SetExitStatus (500);
		}
	}
	//
	// Cleanup and return
	access_fini ();
	ldns_resolver_deep_free (upstream);
	upstream = NULL;
	a2id_dropkeys ();
	exit (0);
}

